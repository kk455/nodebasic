const express = require('express');
const fs = require('fs')
const port=3000;

const expressServer = express();

expressServer.get('/',(req,res)=>{
    res.send('<p>Hello world <p/>')
})

expressServer.get('/local-api',(req,res)=>{
    fs.readFile('./localApi.json','utf8',(err,data)=>{
        if(err)
        {
            console.log(err);
            res.status(500).json({error:'server error'})
            return
        }

        const dataObject = JSON.parse(data)
        res.json(dataObject) 
    });
    
});

let fetch;
(async () => {
  const nodeFetch = await import('node-fetch');
  fetch = nodeFetch.default;

  // Your code using fetch goes here
})();


expressServer.get('/global-api',(req,res)=>{
    fetch('https://dummyjson.com/carts')
        .then( (response) => {
            return response.json();
        })
        .then((data) => {
            const dataObject = JSON.parse(data)
            res.json(dataObject)
        })
        .catch((error) => {
            res.status(500).json({error:'server error'})
        })
});

expressServer.listen(port,() => {
    console.log('listening to requist')
})


// const http = require('http');
// const port=3000;

// const server = http.createServer((req,res)=> {
//     res.setHeader('contain-type','text/html')

//     res.write('<p> Hello World <p/>')

//     res.end()
// });

// server.listen(port,'localhost',()=> {
//     console.log("listen to request");
// })
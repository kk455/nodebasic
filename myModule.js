

const sum = (a,b) => a+b;
const diff = (a,b) => a-b;
const product = (a,b) => a*b;
const ratio = (a,b) => a/b;
const remainder = (a,b) => a%b;



module.exports = {
    sum,
    diff,
    product,
    ratio,
    remainder
}